import ContentModel from './header/ContentModel.js';
import HeaderItemModel from './header/HeaderItemModel.js';
import HomepageModel from './homepage/HomepageModel.js';
import PicksListModel from './staffPicks/PicksListModel.js';

export default {
  ContentModel,
  HeaderItemModel,
  HomepageModel,
  PicksListModel,
};
