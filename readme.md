# Usage

Require `dgx-model-data` as a dependency in `package.json`.

    import { HeaderItemModel, HomepageModel } from 'dgx-model-data';

    // Fetch data
    // Parse the data if it's coming from the Refinery
    const parsedHeaderData = parser.parse(headerData.data, headerOptions);
    const headerModelData = HeaderItemModel.build(parsedHeaderData);

    // At this point `headerModelData` is added to an Alt store, but can be used in other ways.


Currently there are three modeling options, and the main function to use is `build()`.
* HeaderItemModel
* HomepageModel
* PicksListModel

Coming soon: BookListModel

